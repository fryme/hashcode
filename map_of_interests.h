#ifndef MAP_OF_INTERESTS_H
#define MAP_OF_INTERESTS_H

#include <map>
#include <sstream>
#include <set>
#include <vector>
#include <queue>
#include <memory>
#include "pizza.h"


using namespace std;

struct Cell
{
	Cell() : r(0), c(0) {}
	Cell(int r_, int c_) : r(r_), c(c_) {}
	int r, c;
};

struct Slice
{
	Cell leftTop;
	Cell rightBottom;
	
	Slice() {}
	Slice(Cell leftTop_, Cell rightBottom_) :leftTop(leftTop_), rightBottom(rightBottom_) {}
};

struct SliceCompare
{
	bool operator()(const Slice &lhs, const Slice &rhs)
	{
		string lt;
		stringstream slt;
		slt << lhs.leftTop.r << lhs.leftTop.c << lhs.rightBottom.r << lhs.rightBottom.c;
		lt = slt.str().c_str();

		string rb;
		stringstream srb;
		srb << rhs.leftTop.r << rhs.leftTop.c << rhs.rightBottom.r << rhs.rightBottom.c;
		rb = srb.str().c_str();
		return lt < rb;
	}
};

struct Pattern
{
	Pattern(int r_, int c_) : r(r_), c(c_) {}
	int r;
	int c;
};

struct SlicesEnumerator
{
	SlicesEnumerator(/*set<Slice, SliceCompare>*/const vector<Slice>& slices) : m_currentIndex(0)
	{
		m_slices = slices;
		/*for (auto s: slices)
			m_slices.push_back(s);*/
	}

	Slice GetNextSlice()
	{
		return m_slices[0];
	}

	bool IsEmpty()
	{
		return m_slices.empty();
	}

	Slice GetPrevious()
	{
		return m_slices[m_currentIndex - 1];
	}

	int m_currentIndex;
	vector<Slice> m_slices;
};

struct MapOfInterest
{
public:
	MapOfInterest(Pizza& p);

	bool IsDone();
	Slice GetNextSlice();
	void RollbackLastSlice();

	void SetBusy(const Slice& s, bool isBusy = true);
	bool IsBusy(const Cell& c);
	bool IsBusy(const Slice& s);
	Pizza& GetPizza()
	{
		return m_pizza;
	}

private:
	vector<Pattern> GenerateAllPatterns(int sliceSize);
	bool IsPatternSuitable(Pattern& p, int r, int c);
	void RefreshEnumerator();
	vector<Slice> FindAllPossibleSlices();
	bool Intersects(const Slice& left, const Slice& right);

	Pizza m_pizza;
	set<Slice, SliceCompare> m_slices;
	vector<vector<bool>> m_busy;
	shared_ptr<SlicesEnumerator> m_enumerator;
	vector<Slice> selected;
};

/*

IMapOfInterest map;

CheckSlice();

while(!map.IsDone())
{
	Slice& s = map.GetNextSlice();
	// modify s

	map.SetBusy(s);

}

*/

#endif
