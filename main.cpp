//
//  main.cpp
//  PizzaProject
//
//  Created by Nikolay Lozhkarev on 08/02/2017.
//  Copyright © 2017 lozhkarev. All rights reserved.
//

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include "pizza.h"
#include "map_of_interests.h"
#include "alg.h"


Pizza BakePizza(ifstream& input)
{
    size_t R = 0;
    size_t C = 0;
    size_t L = 0;
    size_t H = 0;
    
    input >> R;
    input >> C;
    input >> L;
    input >> H;
    
    Pizza pizza(R, C, L, H);
    
    for (size_t row = 0; row < R; ++row)
    {
        for (size_t col = 0; col < C; ++col)
        {
            char stuff = '?';
            Ingredient i = Unknown1;
            input >> stuff;
            switch (stuff)
            {
                case 'T':
                    i = Tomato;
                    break;
                case 'M':
                    i = Mushroom;
                    break;
            }
            pizza.AddIngredient(row, col, i);
        }
    }
    
    return pizza;
}

void PrintResult(const vector<Slice>& slices)
{
    cout << slices.size() << endl;
    
    for (const Slice& slice : slices)
    {
        cout << slice.leftTop.r << " "<< slice.leftTop.c << " " << slice.rightBottom.r << " " << slice.rightBottom.c << endl;
    }
}

int main(int argc, const char * argv[]) {
    
    if (argc <= 1)
        return -1;
    
    ifstream input(argv[1]);
    
    Pizza pizza = BakePizza(input);
    // Pizza is ready!!
    
//    for (size_t i = 0; i < pizza.GetRowCount(); ++i)
//    {
//        for (size_t j = 0; j < pizza.GetColumnCount(); ++j)
//        {
//            cout << pizza.Get(i, j);
//        }
//        cout << endl;
//    }
    
    MapOfInterest map(pizza);
    
    Algorithm algo(map);
    
    size_t score = 0;
    vector<Slice> slices;
    algo.Calculate(score, slices);
    
    cout << "Score = " << score << endl;
    
    cout << "Result:" << endl;
    
    PrintResult(slices);
    
    return 0;
}
