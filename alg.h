#pragma once

#ifndef __ALG_H__
#define __ALG_H__

#include <stdio.h>


#include <iostream>
#include <vector>
#include <algorithm>

#include "map_of_interests.h"

//
typedef std::vector<Slice> SliceList;

//
class Algorithm
{
public:
	Algorithm(MapOfInterest& mapOfInterests)
		: m_mapOfInterests(mapOfInterests)
	{
	}

	void Calculate(size_t& score, SliceList& slices);

private:
	MapOfInterest& m_mapOfInterests;
};

#endif // __ALG_H__
