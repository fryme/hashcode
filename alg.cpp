#include "alg.h"
#include "map_of_interests.h"

#define COUT std::cout
using std::endl;

//
void Algorithm::Calculate(size_t& score, SliceList& slices)
{
	const Pizza& pizza = m_mapOfInterests.GetPizza();
	const auto RowCount = pizza.GetRowCount();
	const auto ColCount = pizza.GetColumnCount();
	const auto MaxSliceSize = pizza.GetMaxSliceSize();
	const auto MinIngredientsCount = pizza.GetMinIngredientsNumber();

	score = 0;
	slices.clear();
	//slices.reserve(std::min());
	size_t counter = 0;
	while (!m_mapOfInterests.IsDone())
	{
		Slice slice = m_mapOfInterests.GetNextSlice();

		// TODO: All! :)
		score += ((slice.rightBottom.r - slice.leftTop.r + 1) * (slice.rightBottom.c - slice.leftTop.c + 1));
		slices.push_back(slice);
		if (++counter % 100 == 0)
			cout << counter << endl;

		m_mapOfInterests.SetBusy(slice);
	}
}
