#include "map_of_interests.h"
#include <Windows.h>
#include <iostream>
#include <algorithm>
using std::max;
using std::min;

MapOfInterest::MapOfInterest(Pizza& p) : m_pizza(p)
{
	//for (int r = 0; r < m_pizza.GetRowCount(); ++r)
	//{
	//	vector<bool> temp;
	//	temp.resize(m_pizza.GetColumnCount(), false);
	//	m_busy.push_back(temp);
	//}

	RefreshEnumerator();
}

vector<Pattern> MapOfInterest::GenerateAllPatterns(int sliceSize)
{
	vector<Pattern> result;
	int delim = 1;
	int current = sliceSize;
	while (delim <= sliceSize)
	{
		if (current % delim == 0)
			result.push_back({delim, current/delim});
		delim += 1;
	}

	return result;
}

bool MapOfInterest::IsPatternSuitable(Pattern& p, int r, int c)
{
	int tCounter = 0;
	int mCounter = 0;
	for (int rc = r; rc < r + p.r; ++rc)
	{
		for (int cc = c; cc < c + p.c; ++cc)
		{
			//if (m_busy[rc][cc])
			//	return false;

			auto in = m_pizza.Get(rc, cc);
			switch (in)
			{
			case Ingredient::Tomato:
				tCounter++;
				break;
			case Ingredient::Mushroom:
				mCounter++;
				break;
			default:
				throw std::runtime_error("!!!");
			}
		}
	}
	
	return (tCounter == mCounter) && (mCounter == m_pizza.GetMinIngredientsNumber());
}

vector<Slice> MapOfInterest::FindAllPossibleSlices()
{
	//set<Slice, SliceCompare> slices;
	vector<Slice> slices;
	vector<Pattern> patterns = GenerateAllPatterns(m_pizza.GetMinIngredientsNumber() * 2);
	size_t patternCnt = 0;
	for (auto& p : patterns)
	{
		std::cout << "Pattern " << ++patternCnt << std::endl;
		for (size_t r = 0; (r + p.r) <= m_pizza.GetRowCount(); ++r)
		{
			for (size_t c = 0; (c + p.c) <= m_pizza.GetColumnCount(); ++c)
			{
				if (IsPatternSuitable(p, r, c))
				{
					Slice s;
					s.leftTop.r = r;
					s.leftTop.c = c;
					s.rightBottom.r = r + p.r - 1;
					s.rightBottom.c = c + p.c - 1;
					//slices.insert(s);
					slices.push_back(s);
				}
			}
		}
	}
	return slices;
}

bool MapOfInterest::IsDone()
{
	return m_enumerator->IsEmpty();
}

Slice MapOfInterest::GetNextSlice()
{
	return m_enumerator->GetNextSlice();
}

void MapOfInterest::RollbackLastSlice()
{
	SetBusy(m_enumerator->GetPrevious(), false);
	RefreshEnumerator();
}

void MapOfInterest::RefreshEnumerator()
{
	m_enumerator = make_shared<SlicesEnumerator>(FindAllPossibleSlices());
}

bool MapOfInterest::Intersects(const Slice& f, const Slice& s)
{
	//RECT rf, rs;
	//RECT lprcDst;
	//
	//rf.left = f.leftTop.c;
	//rf.top = f.leftTop.r;
	//rf.bottom = f.rightBottom.r;
	//rf.right = f.rightBottom.c;

	//rs.left = s.leftTop.c;
	//rs.top = s.leftTop.r;
	//rs.bottom = s.rightBottom.r;
	//rs.right = s.rightBottom.c;

	//return IntersectRect(&lprcDst, &rf, &rs);

	

	int rowIntersection = max(0, 1 + min(f.rightBottom.r, s.rightBottom.r) - max(f.leftTop.r, s.leftTop.r));
	int colIntersection = max(0, 1 + min(f.rightBottom.c, s.rightBottom.c) - max(f.leftTop.c, s.leftTop.c));
	return rowIntersection * colIntersection > 0;

	/*
	if (f.leftTop.r <= s.rightBottom.r && f.leftTop.r >= s.leftTop.r &&
		f.leftTop.c >= s.leftTop.c && f.leftTop.c <= s.rightBottom.c)
		return true;

	if (s.leftTop.r <= f.rightBottom.r && s.leftTop.r >= f.leftTop.r &&
		s.leftTop.c >= f.leftTop.c && s.leftTop.c <= f.rightBottom.c)
		return true;
	
	Cell cf(f.rightBottom.r, f.leftTop.c);
	Cell cs(s.rightBottom.r, s.leftTop.c);

	if (cf.r <= s.rightBottom.r && cf.r >= s.leftTop.r &&
		cf.c >= s.leftTop.c && cf.c <= s.rightBottom.c)
		return true;

	if (cs.r <= f.rightBottom.r && cs.r >= f.leftTop.r &&
		cs.c >= f.leftTop.c && cs.c <= f.rightBottom.c)
		return true;

	return false;
	*/
}

void MapOfInterest::SetBusy(const Slice& s, bool isBusy)
{
	//for (int r = s.leftTop.r; r <= s.rightBottom.r; ++r)
	//	for (int c = s.leftTop.c; c <= s.rightBottom.c; ++c)
	//		m_busy[r][c] = isBusy;
	
	decltype (m_enumerator->m_slices) temp;
	for (const auto& slice : m_enumerator->m_slices)
	{
		if (!Intersects(slice, s))
		{
			temp.push_back(slice);
		}		
	}
	temp.swap(m_enumerator->m_slices);	
}

bool MapOfInterest::IsBusy(const Cell& c)
{
	return m_busy[c.r][c.c];
}

bool MapOfInterest::IsBusy(const Slice& s)
{
	return false;
}
