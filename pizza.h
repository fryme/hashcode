//
//  pizza.h
//  PizzaProject
//
//  Created by Nikolay Lozhkarev on 08/02/2017.
//  Copyright © 2017 lozhkarev. All rights reserved.
//

#ifndef pizza_h
#define pizza_h

enum Ingredient
{
    Unknown1,
    Tomato,
    Mushroom
};

class Pizza
{
    
public:

    Pizza(size_t rows, size_t columns, size_t ingredientLimit, size_t sliceSizeLimit):
        m_rows(rows),
        m_columns(columns),
        m_minIngredientsCount(ingredientLimit),
        m_maxSliceSize(sliceSizeLimit)
    {
        m_data = new char[m_rows * m_columns];
    }
    
    void AddIngredient(size_t row, size_t col, Ingredient stuff)
    {
        m_data[row * m_columns + col] = stuff;
    }
    
    Pizza(const Pizza& rhs):
        m_rows(rhs.m_rows),
        m_columns(rhs.m_columns),
        m_minIngredientsCount(rhs.m_minIngredientsCount),
        m_maxSliceSize(rhs.m_maxSliceSize)
    {
        m_data = new char[m_rows * m_columns];
        for (size_t i = 0; i < m_rows; ++i)
        {
            for (size_t j = 0; j < m_columns; ++j)
            {
                m_data[i * m_columns + j] = rhs.m_data[i * m_columns + j];
            }
        }
    }
    
    virtual ~Pizza()
    {
        delete [] m_data;
    }
    
    
    Ingredient Get(size_t row, size_t col)
    {
        return (Ingredient)m_data[row * m_columns + col];
    }
    
    size_t GetRowCount() const
    {
        return m_rows;
    }
    
    size_t GetColumnCount() const
    {
        return m_columns;
    }
    
    size_t GetMinIngredientsNumber() const
    {
        return m_minIngredientsCount;
    }
    
    size_t GetMaxSliceSize() const
    {
        return m_maxSliceSize;
    }
    
private:
    
    size_t m_rows;
    size_t m_columns;
    
    size_t m_minIngredientsCount; // L param
    size_t m_maxSliceSize; // H param
    
    
    char * m_data;
};


#endif /* pizza_h */
