// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#include <iostream>
using std::cout;
using std::endl;

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <string>
using std::string;
using std::to_string;

#include <set>
using std::set;

#include <map>
using std::map;

#include <vector>
using std::vector;

#include <stack>
using std::stack;

#include <queue>
using std::queue;

#include <functional>
using std::function;

#include <sstream>

#include <memory>
using std::shared_ptr;
using std::unique_ptr;

#include <algorithm>
using std::find;
using std::find_if;
using std::min;
using std::max;
using std::sort;

#include <chrono>
using std::chrono::high_resolution_clock;
using std::chrono::duration;
using std::chrono::duration_cast;

#include <thread>
using std::thread;

#include <mutex>
using std::mutex;
using std::unique_lock;

#include <math.h>

#include <assert.h>




// TODO: reference additional headers your program requires here
