// hc2017.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CacheFinder.h"
#include "request_table.h"
#include "score_checker.h"

#include "data.h"

void LoadData(ifstream& input, Data& data)
{
	size_t V = 0;
	size_t E = 0;
	size_t R = 0;
	size_t C = 0;
	size_t X = 0;

	input >> V >> E >> R >> C >> X;

	size_t size = 0;

	for (size_t i = 0; i < V; ++i)
	{
		input >> size; 
		data.videos.emplace_back(Video(i, size));
	}

	size_t dcLatency = 0, connectionCount = 0;

	for (size_t i = 0; i < E; ++i)
	{
		input >> dcLatency >> connectionCount;

		Endpoint endpoint(i, dcLatency);
		for (size_t j = 0; j < connectionCount; ++j)
		{
			size_t cacheId = -1, latency = 0;
			input >> cacheId >> latency;
			endpoint.edges.emplace_back(Edge(cacheId, latency));

			std::sort(std::begin(endpoint.edges), std::end(endpoint.edges), [](const Edge& e1, const Edge& e2) { return e1.latency < e2.latency; } );
		}

		data.endpoints.emplace_back(endpoint);
	}

	size_t videoId = -1, endpointId = -1, reqCount = 0;

	for (size_t i = 0; i < R; ++i)
	{
		input >> videoId >> endpointId >> reqCount;
		data.requests.emplace_back(Request(reqCount, data.videos[videoId], data.endpoints[endpointId]));
	}

	for (size_t i = 0; i < C; ++i)
	{
		data.caches.emplace_back(Cache(i, X));
	}
}

void PrintResult(ofstream& output, const Data& data)
{
	const std::vector<Cache>& caches = data.GetCaches();

	size_t cacheCount = std::count_if(std::begin(caches), std::end(caches), [](const Cache& c) { return !c.GetVideos().empty(); } );

	output << cacheCount << '\n';

	for (const Cache& c : caches)
	{
		const std::vector<Video>& videos = c.GetVideos();
		if (videos.empty())
			continue;

		output << c.id << ' ';

		for (const Video& v : videos)
		{
			output << v.id << ' ';
		}

		output << '\n';
	}
}

int main(int argc, const char * argv[])
{
	if (argc < 3)
	{
		cout << "Invalid arguments count" << endl;
		cout << "Try this way: hc2017 <input file> <output file>" << endl;
		return 1;
	}

	ifstream input(argv[1]);
	ofstream output(argv[2]);

	Data data;
	LoadData(input, data);
	
	RequestTable rt(data);
	CacheFinder cf(data, rt);
	cf.FillAllCaches();

	cout << "Score = " << CalculateScore(data) << endl;
	
	cout << "Result:" << endl;

	PrintResult(output, data);

	// Max's test commit
    return 0;
}

