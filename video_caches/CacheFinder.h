#pragma once
#include "stdafx.h"
#include "request_table.h"
#include "data.h"


class CacheFinder
{
	struct ServerRating
	{
		ServerRating():
			serverId(-1),
			rating(0)
		{}

		ServerRating(size_t serverId_, const double& rating_):
			serverId(serverId_),
			rating(rating_)
		{}

		size_t serverId;
		double rating;
	};

public:

	CacheFinder(Data& data, RequestTable& reqTable):
		m_data(data),
		m_reqTable(reqTable)
	{

	}

	void FillAllCaches()
	{
		size_t counter = 0;

		while (!m_reqTable.IsDone())
		{

			if (++counter % 1000 == 0)
				cout << "request # " << counter << endl;

			const Request& rq = m_reqTable.GetNextRequest();
			
			// get endpoint 
			const Endpoint ep = rq.endpoint;

			// get edges for this endpoint
			const vector<Edge>& edges = ep.edges;
			
			// get video for this request
			const Video& video = rq.video;

			// check that video is not on cache server yet
			if (IsVideoCachedAlready(video, edges))
			{
				//cout << "video # " << video.id << " already cached" << endl;
				continue;
			}
			
			// look for best cache server
			const vector<ServerRating> cacheServerRating = RateEdges(edges, video);
			assert(cacheServerRating.size() == edges.size());

			bool isPlaced = false;
			for (const auto& rating : cacheServerRating)
			{
				// assume sorted for latency
				const size_t cacheId = rating.serverId;

				vector<Cache>& caches = m_data.GetCaches();

				Cache& cache = caches[cacheId];
				
				// if cache server is good enough (check size)
				if (cache.GetFreeSpace() > video.size)
				{
					// put this video to the server
					cache.AddVideo(video);
					isPlaced = true;
					break;
				}
			}

			if (!isPlaced)
			{
				//cout << "video # " << video.id << " is not placed" << endl;
			}
		}
	}

	vector<ServerRating> RateEdges(const vector<Edge>& edges, const Video& video)
	{
		map<size_t, double> ratedCacheServers;

		// initialization
		for (const auto& edge : edges)
		{
			const size_t cacheId = edge.cacheId;
			ratedCacheServers[edge.cacheId] = 0;			
		}

		const vector<RequestEx>& requests = m_reqTable.GetRequests();

		vector<size_t> reqindexes = m_reqTable.GetRequestsByVideo(video.id);

		for (auto rqIndex : reqindexes)
		{
			const RequestEx& rq = requests[rqIndex];
			const vector<Edge>& rqEdges = requests[rqIndex].r.endpoint.edges;

			for (const auto& edge : rqEdges)
			{
				auto it = ratedCacheServers.find(edge.cacheId);
				if (it != ratedCacheServers.end())
				{
					it->second += rq.r.reqCount;
				}
			}
		}

		vector<ServerRating> rating;
		for (const auto& rcs : ratedCacheServers)
		{
			rating.push_back(ServerRating(rcs.first, rcs.second));
		}

		sort(rating.begin(), rating.end(), [](const ServerRating& l, const ServerRating& r) { return l.rating >= r.rating; });

		return rating;
	}

	bool IsVideoCachedAlready(const Video& video, const vector<Edge>& edges)
	{
		const vector<Cache> cashes = m_data.GetCaches();

		for (const auto& edge : edges)
		{
			if (cashes[edge.cacheId].IsContainsVideo(video.id))
				return true;
		}

		return false;
	}


private:
	Data& m_data;
	RequestTable& m_reqTable;
};