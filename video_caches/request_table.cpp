#include "stdafx.h"
#include "request_table.h"

RequestTable::RequestTable(const Data& data) : m_data(data), m_currentIndex(-1)
{
	CalculateRates(data.GetRequests());
}

void RequestTable::CalculateRates(const vector<Request>& requests)
{
	vector<RequestEx> requestsV;
	for (const auto& r : requests) 
	{
		
		
		
		//int counter = 0;

		//for (const auto& reqLinked : requests)
		//{
		//	if (reqLinked.video.id == r.video.id)
		//		counter++;
		//}

		
		
		double currentRate = (double) (r.reqCount *r.reqCount) / r.video.size;
		requestsV.push_back(RequestEx(r, currentRate));
	}
	
	RequestComparator rc;
	std::sort(requestsV.begin(), requestsV.end(), rc);

	for (const auto& r : requestsV)
		m_requests.push_back(r);

	// mapping video to requests

	for (size_t i = 0; i < m_requests.size(); ++i)
	{
		m_videoToRequests[m_requests[i].r.video.id].push_back(i);
	}

}

vector<Endpoint> RequestTable::GetLinkedEndpoints(const Video& v)
{
	vector<Endpoint> result;
	
	for (auto r : m_requests)
	{
		if (r.r.video.id == v.id)
			result.push_back(r.r.endpoint);
	}

	return result;
}

const Request& RequestTable::GetNextRequest()
{
	if (m_currentIndex < 0)
		m_currentIndex = 0;
	else
		m_currentIndex++;

	return m_requests[m_currentIndex].r;
}

bool RequestTable::IsDone()
{
	return m_currentIndex == (m_requests.size() - 1);
}
