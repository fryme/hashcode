#pragma once

#include "data.h"
#include <sstream>
#include <iostream>

using namespace std;

int CalculateScore(Data& d, bool verbose = false)
{
	auto caches = d.GetCaches();
	int score = 0;
	
	auto eps = d.GetEndpoints();
	auto rqsts = d.GetRequests();
	uint32_t totalRequests = 0, totalSavedTime = 0;

	for (const auto& req : rqsts)
	{
		vector<int> latencies;

		for (const auto& e : req.endpoint.edges)
		{
			auto found = std::find_if(caches.begin(), caches.end(), [&e](const auto& cache) { return cache.id == e.cacheId; });
			if (found != caches.end())
			{
				// Check that video from request is here
				if (found->IsContainsVideo(req.video.id))
				{
					// If it is here place it to sorted array
					latencies.push_back(req.endpoint.dcLatency - e.latency);
					std::push_heap(latencies.begin(), latencies.end());
				}
			}
		}

		// Select the smallest one from latencies
		// Multiply latency on number of requests
		if (!latencies.empty())
			totalSavedTime += req.reqCount * latencies.back();
		totalRequests += req.reqCount;
	}

	return ((double) totalSavedTime / totalRequests) * 1000;
}
