#pragma once

#include "stdafx.h"
#include "data.h"

struct Request;
struct Video;
struct Endpoint;

struct RequestEx
{
	Request r;
	double rate;

	RequestEx(Request r_, double rate_) : r(r_), rate(rate_) {}
	//RequestEx operator=()
};

struct RequestComparator
{
	bool operator() (const RequestEx& r1, const RequestEx& r2)
	{
		return r1.rate > r2.rate;
	}
};

struct RequestTable
{
	const Request& GetNextRequest();
	bool IsDone();
	vector<Endpoint> GetLinkedEndpoints(const Video& v);
	
	RequestTable(const Data& data);

	void CalculateRates(const vector<Request>& requests);
	
	const vector<RequestEx>& GetRequests() const
	{
		return m_requests;
	}

	const vector<size_t>& GetRequestsByVideo(size_t video_id)
	{
		return m_videoToRequests[video_id];
	}
	
	vector<RequestEx> m_requests;
	const Data& m_data;
	int m_currentIndex;

	// mapping video to requests
	map<size_t, vector<size_t>> m_videoToRequests;
};
