#pragma once

#include <vector>
#include <fstream>
#include <algorithm>

struct Video
{
	Video(size_t id_, size_t size_)
		: id(id_), size(size_)
	{
	}

	 size_t id;
	 size_t size;
};

struct Edge
{
	Edge(size_t cacheId_, size_t latency_)
		: cacheId(cacheId_), latency(latency_)
	{
	}

	size_t cacheId;
	size_t latency;
};

struct Endpoint
{
	explicit Endpoint(size_t id_, size_t dcLatency_)
		: id(id_), dcLatency(dcLatency_)
	{
	}

	 size_t id;
	 size_t dcLatency;
	std::vector<Edge> edges; // sorted by latency
};

struct Request
{
	Request(size_t reqCount_, const Video& video_, const Endpoint& endpoint_)
		: reqCount(reqCount_), video(video_), endpoint(endpoint_)
	{
	}

	size_t reqCount;
	Video video;
	Endpoint endpoint;
	std::vector<Endpoint> otherEndpoints;
};

class Cache
{
public:
	const size_t id;
	const size_t capacity;

	Cache()
		: id(-1)
		, capacity(0)
		, freeSpace(0)
	{
	}

	Cache(size_t id_, size_t capacity_)
		: id(id_)
		, capacity(capacity_)
		, freeSpace(capacity_)
	{
	}

	void AddVideo(const Video& video)
	{
		if (freeSpace < video.size)
			throw std::runtime_error("Not enough free space");

		freeSpace -= video.size;
		videos.emplace_back(video);
	}

	const std::vector<Video>& GetVideos() const 
	{
		return videos;
	}

	bool IsContainsVideo(size_t videoId) const
	{
		const auto endIter = std::end(videos);

		return (std::find_if(std::begin(videos), endIter, 
			[videoId](const Video& v) { return v.id == videoId; }))
				!= endIter;
	}

	size_t GetFreeSpace() const
	{
		return freeSpace;
	}

private:
	size_t freeSpace;
	std::vector<Video> videos;
};

class Data
{
	friend void LoadData(std::ifstream& input, Data& data);

public:
	const std::vector<Video>& GetVideos() const
	{
		return videos;
	}

	const std::vector<Endpoint>& GetEndpoints() const
	{
		return endpoints;
	}

	const std::vector<Request>& GetRequests() const
	{
		return requests;
	}

	std::vector<Cache>& GetCaches()
	{
		return caches;
	}

	const std::vector<Cache>& GetCaches() const
	{
		return caches;
	}

private:
	std::vector<Video> videos;
	std::vector<Endpoint> endpoints;
	std::vector<Request> requests;
	std::vector<Cache> caches;
};